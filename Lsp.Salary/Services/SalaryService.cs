﻿using Lsp.Salary.Models;

namespace Lsp.Salary.Services
{
	public class SalaryService
	{
		public decimal CalculateBonus(Employee employee, decimal basicRate)
		{
			if (employee is Manager manager)
			{
				return manager.YearExperience * basicRate + manager.Subordinates.Count * basicRate * 0.3m;
			}

			return employee.YearExperience * basicRate;
		}
	}
}