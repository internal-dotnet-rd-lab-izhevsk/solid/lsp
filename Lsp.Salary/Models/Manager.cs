﻿using System.Collections.Generic;

namespace Lsp.Salary.Models
{
	public class Manager : Employee
	{
		public ICollection<Employee> Subordinates { get; set; }
	}
}