﻿using System;

namespace Lsp.Salary.Models
{
	public class Employee
	{
		public string FullName { get; set; }
		public DateTime BirthDate { get; set; }
		public int YearExperience { get; set; }
	}
}