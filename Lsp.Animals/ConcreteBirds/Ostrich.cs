﻿using System;

namespace Lsp.Animals.ConcreteBirds
{
	public class Ostrich : Bird
	{
		public override void Fly()
		{
			throw new InvalidOperationException("I'm sorry, I can't fly, but I can bury my head in the sand");
		}

		public override void Shout()
		{
			Console.WriteLine("yoi-yoi-yoi!!!");
		}
	}
}