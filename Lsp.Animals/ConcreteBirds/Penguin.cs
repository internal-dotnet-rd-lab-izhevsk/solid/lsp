﻿using System;

namespace Lsp.Animals.ConcreteBirds
{
	public class Penguin : Bird
	{
		public override void Fly()
		{
			throw new InvalidOperationException("I'm sorry, I can't fly, but I can swim");
		}

		public override void Shout()
		{
			Console.WriteLine("ehw-ehw-ehw!!!");
		}
	}
}