﻿using System;

namespace Lsp.Animals
{
	public abstract class Bird
	{
		public virtual void Fly()
		{
			Console.WriteLine("I fly");
		}

		public abstract void Shout();
	}
}